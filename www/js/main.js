$(document).ready(function(){

    $(window).scroll(function(){

        var scroll = $(window).scrollTop();
        if(scroll>65){
            $(".top-button").show();
        }
        else{
            $(".top-button").hide();
        }
        console.log(scroll);

    })

    $( ".menu-trigger" ).click(function() {
        $( ".menu" ).toggle(500);
        $( "body" ).toggleClass( "scroll-off" );
    });
});

var t;
function up() {
    var top = Math.max(document.body.scrollTop,document.documentElement.scrollTop);
    if(top > 0) {
        window.scrollBy(0,-100);
        t = setTimeout('up()',10);
    } else clearTimeout(t);
    return false;
}

